const PagesModel = require("../model/Page");

module.exports = {
    getPage: async (req, res, next) => {
        try{
            let pages = await PagesModel.getPage(req.params.page);
            res.render("page", {page: pages});
        } catch ( err ){
            console.log(err);
            res.status(500).send("erreur interne");
        }
    },
    addPage: async (req, res, next) => {
        try{
            let pages = await PagesModel.addPage(req, res);
        } catch ( err ){
            console.log(err);
            res.status(500).send("erreur interne");
        }
    }
}