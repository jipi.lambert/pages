const Mongoose = require("mongoose");

const Schema = Mongoose.Schema;

let PageSchema = new Schema({
    name: String,
    title: String,
    content: String,
}, {
    timestamps: true
});
const pagesModel = Mongoose.model("Page", PageSchema);

module.exports = {
    model: pagesModel,
    getAllPages : async ()=> {
        return await pagesModel.find({});
    },
    getPage :  async (pagename)=> {
        return await pagesModel.find({name: pagename});
    },
    addPage : async(req, res) =>{
        let data = new pagesModel(req.body);
        data.save(function (err, data) {
            if (err) console.error(err);
            return true;
        })
    }
};