const express = require('express');
const router = express.Router();
const pageController = require('../controllers/PagesController');
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/', function(req, res, next) {
  pageController.addPage(req);
  res.render('index', { title: 'Express' });
});

router.get('/:page', pageController.getPage );

module.exports = router;
